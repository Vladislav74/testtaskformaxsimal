//
//  AppDelegate.h
//  testTaskForMaxsimal
//
//  Created by Владислав on 15.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

