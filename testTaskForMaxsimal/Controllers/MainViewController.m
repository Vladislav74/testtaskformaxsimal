//
//  MainViewController.m
//  testTaskForMaxsimal
//
//  Created by Владислав on 15.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import "MainViewController.h"

#import "Foursquare2.h"
#import "FSVenue.h"
#import "FSConverter.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "VenueDetailView.h"

#import "DXAnnotationSettings.h"
#import "DXAnnotationView.h"

@interface MainViewController () <CLLocationManagerDelegate, MKMapViewDelegate> {
    
    CLLocationManager *locationManager;
    NSArray *nearbyVenues;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.map.delegate = self;

    locationManager = [[CLLocationManager alloc]init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined ) {
            
            [locationManager requestWhenInUseAuthorization];
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                   [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {

            [locationManager startUpdatingLocation];
        } else {

            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"No athorization"
                                                                           message:@"Please, enable access to your location"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * action) {}];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                                handler:^(UIAlertAction * action) {
                                                                   [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                                }];
            
            [alert addAction:okAction];
            [alert addAction:cancelAction];

            [self presentViewController:alert animated:YES completion:nil];
        }
    } else {
        [locationManager startUpdatingLocation];
    }
}

- (IBAction)segmentControlAction:(id)sender {
   [locationManager startUpdatingLocation];
}

#pragma mark - location
- (void)getVenuesForLocation:(CLLocation *)location {
    
    const CLLocationDistance radiusForVenues = [self radiusAtSelectedSegment];
    
    [Foursquare2 venueSearchNearByLatitude:@(location.coordinate.latitude)
                                 longitude:@(location.coordinate.longitude)
                                     query:nil
                                     limit:[NSNumber numberWithInteger:100]
                                    intent:intentBrowse
                                    radius:@(radiusForVenues)
                                categoryId:@"4d4b7105d754a06374d81259" //for cafe and restaurants
                                  callback:^(BOOL success, id result){
                                      if (success) {
                                          NSDictionary *dic = result;
                                          NSArray *venues = [dic valueForKeyPath:@"response.venues"];
                                          FSConverter *converter = [[FSConverter alloc]init];
                                          nearbyVenues = [converter convertToObjects:venues];
                                          [self proccessAnnotations];
                                      }
                                  }];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [locationManager stopUpdatingLocation];
    [self getVenuesForLocation:newLocation];
    [self setupMapForLocatoion:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"locationManager is fail"
                                                                   message:@"some error "
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * action) {
                                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                         }];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
}

-(void)venueGetWorksHours:(NSString *)venueId  Completetion:(void (^)(NSString * worksHours))callbackBlock {

    [Foursquare2 venueGetDetail: venueId
                       callback:^(BOOL success, id result){
                           if (success) {
                               
                               NSDictionary *response = [result valueForKey:@"response"];
                               NSDictionary *venue = [response valueForKey:@"venue"];
                               
                               NSDictionary *hours = [venue valueForKey:@"hours"];
                               
                               NSString *string = @"";
                               
                               if(hours != nil) { //hours may be nil for some cafe
                                   
                                   NSArray *timeframes = [hours valueForKey:@"timeframes"];
                                   
                                   for(NSDictionary *timeframe in timeframes) {
                                       
                                       string = [string stringByAppendingString:[timeframe valueForKey:@"days"]];
                                       NSDictionary *open = [timeframe valueForKey:@"open"];
                                       NSArray *renderedTime = [open valueForKey:@"renderedTime"];
                                       string = [string stringByAppendingString:@" - "];
                                       string = [string stringByAppendingString:renderedTime[0]];
                                   }
                               } else {
                                   string = @"no data";
                               }
                               callbackBlock(string);
                           }
                       }];
}

#pragma mark - radius for selected segment
- (CLLocationDistance) radiusAtSelectedSegment {
    CLLocationDistance radius = 500;
    
    if(self.segmentControl.selectedSegmentIndex == 1) {
        radius = 1000;
    } else if(self.segmentControl.selectedSegmentIndex == 2) {
        radius = 5000;
    }
    return radius;
}

- (CLLocationDistance) mapRadiusAtSelectedSegment {
    const CLLocationDistance radius = [self radiusAtSelectedSegment];
    return (radius * 2.2f);
}

#pragma mark - MAP
- (void)setupMapForLocatoion:(CLLocation *)newLocation {
    
    const CLLocationDistance radius = [self mapRadiusAtSelectedSegment];
    
    MKCoordinateRegion rgn = MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude), radius, radius);
    
    [self.map setRegion:rgn animated:YES];
}

- (void)removeAllAnnotationExceptOfCurrentUser {
    NSMutableArray *annForRemove = [[NSMutableArray alloc] initWithArray:self.map.annotations];
    if ([self.map.annotations.lastObject isKindOfClass:[MKUserLocation class]]) {
        [annForRemove removeObject:self.map.annotations.lastObject];
    } else {
        for (id <MKAnnotation> annot_ in self.map.annotations) {
            if ([annot_ isKindOfClass:[MKUserLocation class]] ) {
                [annForRemove removeObject:annot_];
                break;
            }
        }
    }
    
    [self.map removeAnnotations:annForRemove];
}

- (void)proccessAnnotations {
    [self removeAllAnnotationExceptOfCurrentUser];
    [self.map addAnnotations:nearbyVenues];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[FSVenue class]]) {
        
        UIImageView *pinView = nil;
        
        UIView *calloutView = nil;
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        if (!annotationView) {
            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin"]];
            calloutView = [[[NSBundle mainBundle] loadNibNamed:@"VenueDetailView" owner:self options:nil] firstObject];
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:calloutView
                                                                 settings:[DXAnnotationSettings defaultSettings]];
        }else {
            pinView = (UIImageView *)annotationView.pinView;
        }
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        [((DXAnnotationView *)view)hideCalloutView];
        view.layer.zPosition = -1;
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if ([view isKindOfClass:[DXAnnotationView class]]) {
        
        const FSVenue *selected = self.map.selectedAnnotations.lastObject;
        
        const Boolean isAuthorized = [Foursquare2 isAuthorized];
        if (!isAuthorized) {
            [Foursquare2 authorizeWithCallback:^(BOOL firstSuccess, id firstResult) {
                if (firstSuccess) {
                    [Foursquare2  userGetDetail:@"self"
                                       callback:^(BOOL secondSuccess, id secondResult){
                                           [self venueGetWorksHours:selected.venueId Completetion:^(NSString *worksHours) {
                                               ((VenueDetailView *)((DXAnnotationView *)view).calloutView).workHours.text = worksHours;
                                           }];
                                       }];
                }
            }];
        } else {
            [self venueGetWorksHours:selected.venueId Completetion:^(NSString *worksHours) {
                ((VenueDetailView *)((DXAnnotationView *)view).calloutView).workHours.text = worksHours;
            }];
        }
        
        ((VenueDetailView *)((DXAnnotationView *)view).calloutView).name.text = selected.name;
        [((DXAnnotationView *)view)showCalloutView];
        view.layer.zPosition = 0;
    }
}
@end

