//
//  main.m
//  testTaskForMaxsimal
//
//  Created by Владислав on 15.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
