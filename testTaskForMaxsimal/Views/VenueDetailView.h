//
//  VenueDetailView.h
//  testTaskForMaxsimal
//
//  Created by Владислав on 15.09.16.
//  Copyright © 2016 vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface VenueDetailView : MKAnnotationView

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextView *workHours;

@end
